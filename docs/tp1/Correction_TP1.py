#!/usr/bin/env python
# coding: utf-8

# # Opérations sur les littéraux, variables et affectations

# ## Exercice 1

# In[119]:


print(Hello world)


# Les chaînes de caractères doivent être déimitées par des quotes `'ma chaine'` ou des guillemets `"ma chaine"` !

# In[120]:


print("Hello world")
#Ceci est un commentaire pas interprété par Python


# ## Exercice 2

# In[121]:


trace = True
a = 5
if trace:
    print(f"Trace :  a = {a}")
a = a + 1
if trace:
    print(f"Trace :  a = {a}")
b = a 
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
b = b ** 2 - a
if trace:
    print(f"Trace :  a = {a} et b = {b}")


# In[122]:


trace = True
a = 5
if trace:
    print(f"Trace :  a = {a}")
b = 6
if trace:
    print(f"Trace :   a = {a} et b = {b}")
a = a - b
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
b =b + a
if trace:
    print(f"Trace :  a = {a} et b = {b}")
a = b - a
if trace:
    print(f"Trace :  a = {a} et b = {b}")


# In[123]:


from random import randint
trace = True
a = randint(1, 100) #entier aléatoire entre 1 et 100
if trace:
    print(f"Trace :  a = {a}")
b = randint(1, 100) #entier aléatoire entre 1 et 100
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
a = a - b
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
b = b + a
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
a = b - a
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
print(a, b)


# ## Exercice 3

# In[124]:


# entrées
a = int(input('a ?'))
b = int(input('b ?'))
# traitement
a = b
b = a 
# sorties
print("a = ", a, " et b = ", b)


# Non ce programme ne permute pas les valeurs des variables `a` et `b`, la valeur initiale de `a` est écrasée par l'affectation `a = b`

# In[125]:


#on trace ou non
trace = True
# entrées
a = int(input('a ?'))
b = int(input('b ?'))
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
# traitement
a = b
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
b = a 
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
# sorties
print("a = ", a, " et b = ", b)


# Programme modifié et corrigé, on utilise une variable de stockage `tmp`.

# In[126]:


#on trace ou non
trace = True
# entrées
a = int(input('a ?'))
b = int(input('b ?'))
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
# traitement
tmp = a   #on sauvegarde la valeur initiale de a
a = b
if trace:
    print(f"Trace :  a = {a} et b = {b} et tmp = {tmp} ")
b = tmp   #on affecte à b la valeur initiale de a 
if trace:
    print(f"Trace :  a = {a} et b = {b} et tmp = {tmp} ")
# sorties
print("a = ", a, " et b = ", b)


# En python, on peut se passer de variable de stockage avec __l'affectation parallèle__ ou `tuple unpacking`

# In[127]:


#on trace ou non
trace = True
# entrées
a = int(input('a ?'))
b = int(input('b ?'))
if trace:
    print(f"Trace :  a = {a} et b = {b} ")
# traitement
a, b  = b, a
if trace:
    print(f"Trace :  a = {a} et b = {b}")
# sorties
print("a = ", a, " et b = ", b)


# ## Exercice 4

# In[128]:


def jour_semaine(m, d, y):
    """
    Paramètres : 
        m un entier représentant le mois dans l'année 1<=m<=12
        d un entier représentant le jour  dans le mois 1<=d<=31
        y un entier représentant l'année  
    Valeur renvoyée :
        un entier d0 représentant le rang du jour de la semaine entre 1 et 7
    """
    y0 = y - (14 - m)//12
    x = y0 + y0//4 - y0//100 + y0//400
    m0 = m + 12*((14-m)//12) - 2
    d0 = (d + x + (31 * m0)//12)%7
    return d0

#tests unitaires 
assert jour_semaine(2,14,2000) == 1
assert jour_semaine(2,14,1900) == 3
assert jour_semaine(7,14,1789) == 2


# #  Import de bibliothèque et boucles inconditionnelles

# ## Exercice 5

# In[132]:


from turtle import *   #import du module turtle

def spirale_polygone(cote, tour):
    """
    Signature :spirale_polygone(cote:int, tour:int)->None
    Postcondition : trace une spirale avec cote segments par tour
    , tour est le nombre de tours complets
    """
    pas = 5  #pas initial valeur arbitraire
    increment = 5 #incrémentpour chaque pas, valeur arbitraire
    angle = 360 / cote  #angle de rotation à chaque pas
    for i in range(tour):
        for j in range(cote):
            forward(pas)
            pas = pas + increment
            left(angle)
            
try:
    spirale_polygone(6, 5)  #appel de fonction
    exitonclick()           #boucle gestionnaire d'événements
except:
    pass


# # Exercice 6

# In[139]:


from turtle import *   #import du module turtle


def grille(n, ecart):
    """
    Signature :grille(n:int, ecart:int)->None
    Postcondition : trace une grille orthogonale n x n  à cellules carrées
    """
    #on trace en serppentant
    for tour in range(2):        
        for lig in range(n + 1):       
            forward(n * ecart)            
            if lig < n:
                if lig % 2 == tour:
                    left(90)
                    forward(ecart)
                    left(90)
                else:
                    right(90)
                    forward(ecart)
                    right(90)
        if n % 2 == 0:
            right(90)
        else:
            forward(-n * ecart)
            left(90)
  
        
try:
    grille(7, 50)
    exitonclick()
except:
    pass


# In[144]:


from turtle import *

def spirale(n):
    """
    Signature : spirale(n:int)->None
    Trace une spirale de n tours soit 2 * n demi-cercles en 
    """
    epaisseur = 2
    for _ in range(2 * n):
        width(epaisseur)
        circle(epaisseur ** 2, 180) #help(circle) pour la documentation de la fonction circle
        epaisseur = epaisseur + 1 

try:
    spirale(6)
    exitonclick()
except:
    pass


# # Expressions booléennes et structures conditionnelles

# ## Exercice 7

# In[ ]:


def intersection_intervalle(a,b,c,d):
    """
    Signature : intersection_intervalle(a:float,b:float,c:float,d:float)->bool
    Précondition : vérifier si les intervalles [a,b] et [c,d] sont valides avec un assert
    Postcondition : Détermine si les intervalles [a,b] et [c,d] ont une intersection non vide
    """
    assert a <= b and c <= d
    return not(b < c or d < a)

# Test unitaires
assert not intersection_intervalle(843, 844, 841, 842)
assert intersection_intervalle(840, 841, 841, 842)


def intersection_rectangle(x1min,x1max,y1min,y1max,x2min,x2max,y2min,y2max):
    """
    Signature : intersection_rectangle(x1min:float,x1max,y1min,y1max,x2min,x2max,y2min,y2max)->bool
    Postcondition : Détermine si les rectangles (x1min,x1max,y1min,y1max) et (x2min,x2max,y2min,y2max)
    ont une intersection
    """
    return intersection_intervalle(x1min,x1max,x2min,x2max) and intersection_intervalle(y1min,y1max,y2min,y2max)


# ## Exercice 8

# In[145]:


def bissextile(a):
    """Signature : bissextile(a:int)->bool
    Postcondition : Détermine si l'année a est bissextile"""
    return (a % 100 != 0 and a % 4 == 0) or (a % 400 == 0)

#tests unitaires
assert bissextile(2020)
assert not bissextile(2019)
assert bissextile(2000)
assert not bissextile(19000)


# # Boucles inconditionnelles ou bornées

# ## Exercice 9

# In[146]:


trace = True
f = 0
g = 1
for i in range(12):
    if trace:
        print(f"f = {f} et g={g}")
    f = f + g
    g = f - g


# On reconnaît la suite de Fibonacci

# # Exercice 10

# In[149]:


def factorielle(n:int)->int:
    """Précondition : n >= 0 
    Postcondition : renvoie n!=n(n-1)...3.2 et 0! = 1"""
    assert n >= 0 #précondition
    f = 1
    for k in range(2, n + 1):
        f = f * k
    return f

#tests unitaires
assert factorielle(0) == 1
assert factorielle(1) == 1
assert factorielle(3) == 6


# In[151]:


def somme_puissance(a:int,b:int,n:int)->int:
    """Renvoie a^{n} + (a+1)^{n} + .... + b^{n}."""
    assert a <= b  #précondition
    s = 0
    for k in range(a, b + 1):
        s = s + k ** n
    return s


# In[ ]:


def max2(a:int,b:int)->int:
    """Renvoie le maximum de a et b"""
    if a >= b:
        return a
    else:   #on peut se passer du else car return dans la clause du if
        return b


# In[ ]:


def max3(a:int,b:int,c:int)->int:
    """Renvoie le maximum de a, b et c"""
    if a >= b:
        if a >= c:
            return a
        else:
            return c
    else:
        if b >= c:
            return b
        else:
            return c

def max3V2(a:int,b:int,c:int)->int:
    """Renvoie le maximum de a, b et c"""
    return max(max(a,b),c)


# ## Exercice 11

# In[163]:


def projet_euler1():
    """Renvoie la somme des multiples de 3 ou de 5
    inférieurs à 100"""
    s = 0
    for k in range(3, 100):
        if  k%3 == 0  or k%5 == 0:
            s = s + k
    return s


# In[164]:


projet_euler1()


# ## Exercice 12

# In[165]:


def damier(n:int)->None:
    """Affiche un damier avec ' ' pour noir 
    et '*' pour blanc"""
    for i in range(n):
        ligne = ''
        for j in range(n):
            if (i + j) % 2 == 0:
                ligne = ligne + '*'
            else:
                ligne = ligne + ' '
        print(ligne)


# ## Exercice 13

# In[168]:


def est_premier(n:int)->bool:
    """Détermine si n est premier en au plus n divisions""" 
    assert n >= 0  #précondition
    if n <= 1:
        return False
    for d in range(2, n):
        if n % d == 0:
            return False
    return True


# In[173]:


import math

def est_premier2(n:int)->bool:
    """Détermine si n est premier en au plus racine(n) divisions""" 
    assert n >= 0  #précondition
    if n <= 1:
        return False
    for d in range(2, int(math.sqrt(n)) + 1):
        if n % d == 0:
            return False
    return True


# # Boucles conditionnelles ou non bornées

# ## Exercice 14

# In[ ]:


def somme_chiffres(n:int)->int:
    """Précondition : n >= 0
    Postcondition : sommes des chiffres de n"""
    s = 0   #acumulateur
    while n > 0:
        s = s + (n % 10) #on ajoute le chiffre des unités à s
        n = n // 10   #on enlève le chiffre des unités
    return s


# ## Exercice 15 Paradoxe des anniversaires

# In[174]:


def seuil_anniversaire()->int:
    """Paramètre :  aucun
    Valeur renvoyée : le nombre minimal d'élèves dans une classe
    à partir duquel la probabilité d'avoir au moins 2 élèves avec la 
    même date d'anniversaire est >= 0.5"""
    proba_contraire = 1
    eleve = 1
    while proba_contraire >= 0.5:
        eleve = eleve + 1
        proba_contraire = proba_contraire * (365 - (eleve-1)) / 365        
    return eleve


# In[175]:


seuil_anniversaire()


# ## Exercice 16 : Molkky

# In[ ]:


def nouveau_score(score:int, points:int)->int:
    """Paramètres : le score précédent et le nombre de points obtenus
    Valeur renvoyée : le nouveau nombre de points"""
    assert 0 <= points and points <= 12
    cumul =  score + points
    if cumul > 51:
        return 25
    else:
        return cumul


# In[ ]:


def partie_molkky():
    scoreA = 0
    scoreB = B
    while scoreA != 51 and scoreB != 51:
        scoreA = nouveau_score(scoreA, randint(1,12))
        scoreB = nouveau_score(scoreB, randint(1,12))
    if scoreA > scoreB:
        print("A vainqueur")
    else:
        print("B vainqueur")


# # Exercice 17  Ruine du joueur

# In[176]:


import random

def ruine_joueur(mise:int, objectif:int, nbessais:int)->[float, int]:
    """Simule nbessais parties où un joueur dispose d'une mise 
    et joue tant qu'il le peut ou qu'il atteint son objectif. Une partie est constituée de
    tours où le joueur gagne ou perd 1 euros à pile ou face """
    assert mise >0 and objectif > mise and nbessais > 0  #préconditions
    paris = 0
    victoires = 0
    for _ in range(nbessais):
        gain  = mise
        while (gain > 0) and (gain < objectif):
            paris = paris + 1
            if random.randint(0, 1) == 1:
                gain = gain + 1
            else:
                gain = gain - 1
        if gain == objectif:
            victoires = victoires + 1
    return [victoires / nbessais, paris // nbessais]


# In[177]:


ruine_joueur(10, 20, 1000)


# In[178]:


ruine_joueur(500, 2500, 100)


# In[ ]:


import random
import time

def ruine_joueur2(mise:int, objectif:int, nbessais:int)->[float, int]:
    """Simule nbessais parties où un joueur dispose d'une mise 
    et joue tant qu'il le peut ou qu'il atteint son objectif. Une partie est constituée de
    tours où le joueur gagne ou perd 1 euros à pile ou face """
    assert mise >0 and objectif > mise and nbessais > 0  #préconditions
    debut = time.perf_counter()
    paris = 0
    victoires = 0
    for _ in range(nbessais):
        gain  = mise
        while (gain > 0) and (gain < objectif):
            paris = paris + 1
            if random.randint(0, 1) == 1:
                gain = gain + 1
            else:
                gain = gain - 1
        if gain == objectif:
            victoires = victoires + 1
    temps = time.perf_counter() - debut
    return [victoires / nbessais, paris // nbessais, temps / paris]


# In[ ]:


ruine_joueur(500, 2500, 100)


# In[ ]:


ruine_joueur(500, 2500, 100)


# ## Exercices avancés 

# In[ ]:


from turtle import *

def koch(n):
    chemin = "F"
    for k in range(n):
        chemin2 = ""
        for c in chemin:
            if c == "F":
                chemin2 = chemin2 + "F+F-F+F"
            else:
                chemin2 = chemin2 + c
        chemin = chemin2
    return chemin


def trace_koch(n, pas):
    chemin = koch(n)
    up()
    goto(0,0)
    speed(0)
    down()
    for c in chemin:
        if c == "F":
            forward(pas)
        elif c == "+":
            left(60)
        else:
            right(120)

def trace_koch2(n, pas):
    chemin = koch(n)
    for c in chemin:
        if c == "F":
            forward(pas)
        elif c == "+":
            left(60)
        else:
            right(120)
                        
def trace_flocon_koch(n, pas):
    up()
    goto(-500,200)
    speed(0)
    down()
    for k in range(3):
        trace_koch2(n, pas)
        right(120)
    figure = getcanvas()
    figure.postscript(file = "floconKoch.png", colormode = "color")
    exitonclick()


# ## Exercice 19 Conjecture d’Euler sur la somme des puissances

# Voir <https://en.wikipedia.org/wiki/Euler%27s_sum_of_powers_conjecture>

# In[ ]:


def recherche_contre_exemple(bsup):
    for d in range(4, bsup ):
        for c in range(3, d):
            for b in range(2, c):
                for a in range(1, b):
                    s = a ** 5 + b ** 5 + c ** 5 + d ** 5
                    e = d
                    while e ** 5 < s:
                        e = e + 1
                        #print(e)
                    if e ** 5 == s:
                        return (a,b,c,d,e)
                    
recherche_contre_exemple(150)


# ## Exercice 20 suite de Conway

# In[182]:


def suivant(preced:str)->str:
    """Postcondition : renvoie le terme suivant
    de la suite audioactive de Conway"""
    assert len(preced) > 0 #précondition
    suiv = ''  #chaine vide
    courant = preced[0] 
    compteur = 1
    for k in range(1, len(preced)):
        if preced[k] != courant:
            suiv = suiv + str(compteur) + courant
            courant = preced[k]
            compteur = 1
        else:
            compteur = compteur + 1
    suiv = suiv + str(compteur) + courant
    return suiv

def conway(n:int)->str:
    """Précondition : n >= 0
    Postcondition : renvoie les n premiers termes de la suite 
    audioactive de Conway, séparés par des sauts de ligne"""
    terme = '1'
    for _ in range(n):
        print(terme)
        terme = suivant(terme)


# In[187]:


conway(14)

