---
title: Thème Fonctions, opérations arithmétiques
---

{% include 'abbreviations.md' %}

Exercice tiré de  [Progalgo](https://progalgo.fr/) de Julien de Villèle.

!!! tip "Exercice"

    *Remarque :* Pour les calculs de puissance on utilise l'opérateur `**`. Par exemple $5^2 = 25$ se calcule grâce à `5**2`.  



    *Question 1 :* Compléter la fonction `aire_disque` suivante pour qu'elle renvoie l'aire d'un disque à partir de son rayon `r`.  
    On rappelle que l'aire $A$ d'un disque de rayon $r$ est donnée par $A = \pi \times r^2$.

    ~~~python
    import math  #math.pi pour avoir pi

    def aire_disque(r): 
        """Signature aire_disque(r:float)->float"""       
        return "à compléter"
    ~~~


{{IDE("exo1/exo1_fonctions")}} 