---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de deux lignes de code au plus,  qui affiche tous les entiers entre 10 et 0 inclus dans l'ordre décroissant (un par ligne).
    


{{IDE("exo3/exo3_range")}} 

[Correction](scripts/exo3/corr_exo3_range.py)