#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TP4 : recherche de motif
"""
#%% Exercice 1

def est_croissant(tab):
    """
    Détermine si le tableau de nombres tab est dans l'ordre croissant
    Parameters:
        tab : tableau de nombres
        Précondition : len(tab) > 0
    Return :
        boolean
    """
    # Précondition
    assert len(tab) > 0 
    # à compléter
    # BEGIN CUT
    for k in range(len(tab) - 1):
        if tab[k] > tab[k+1]:
            return False
    return True
    # END CUT

# Tests unitaires
assert est_croissant([1])
assert est_croissant([1, 1])
assert est_croissant([-1, 0, 4])
assert est_croissant([-1, 4, 4])
assert not est_croissant([4, 5, 3])
assert not est_croissant([4, 3, 7])
assert not est_croissant([-1, 0, 2, 1, 4, 5])

#%% Exercice 2


# Tableau carre contenant les carrés des entiers successifs entre −3 et 5 inclus
# BEGIN CUT
carre = [n ** 2 for n in range(-3, 6)]
# END CUT

def somme_cumulees(tab):
    """
    Renvoie le tableau des sommes cumulées
    Complexité :  len(tab) affectations et additions

    Parameters:
        tab  : tableau d'entiers

    Returns:
        tableau d'entiers
    """
    # à compléter
    # BEGIN CUT
    cumul = [0 for _ in range(len(tab))]
    cumul[0] = tab[0]
    for k in range(1, len(tab)):
        #cumul[k-1] = sum(tab[0:k])  invariant 
        cumul[k] = cumul[k - 1] + tab[k]
    return cumul
    # END CUT


# Jeu de tests unitaires
assert somme_cumulees([1]) == [1]
assert somme_cumulees([1, -3]) == [1, -2]
assert somme_cumulees([1, 2, 3, 4]) == [1, 3, 6, 10]

#%% Exercice 3

def miroir(chaine):
    """
    Paramètre : chaine de type str
    Valeur renvoyée : une chaîne de caractères de type str
    Postcondition : inverse la chaîne prise en entrée
    """
    # à compléter    
    reflet = ""
    # BEGIN CUT
    for c in chaine:
        reflet = c + reflet
    # END CUT
    return reflet

# Tests unitaires
assert miroir("Suis-je toujours la plus belle?") == "?elleb sulp al sruojuot ej-siuS"
assert miroir("ABBA") == "ABBA"
assert miroir("Z") == "Z"
assert miroir("KAYAK") == "KAYAK"

def palindrome(chaine):
    """
    Détermine si une chaine de caractère est un palindrome

    Parameters:
        chaine : str

    Returns:
        boolean
    """
    # à compléter
    # BEGIN CUT
    n = len(chaine)
    for k in range(n//2):
        if chaine[k] != chaine[n-1-k]:
            return False
    return True
    # END CUT

# Tests unitaires
assert palindrome("sator arepo tenet opera rotas")
assert palindrome("KAYAK")

#%% Exercice 4


def valeur(mot):
    """Paramètre : un mot de type str
    Précondition : mot non vide
    Valeur renvoyée : un entier
    Postcondition : produit des points de code des lettres de mot    
    """
    assert len(mot) > 0 #précondition
    #à compléter
    #BEGIN CUT
    v = 1
    for c in mot:
        rang = ord(c) - ord('a') + 1
        v = v * rang
    return v
    # END CUT

# Tests unitaires
assert valeur("pcsi") == 8208
assert valeur("bac") == 6
assert valeur("dictionnaire") == 46294416000

def indice_maximum(tab):
    """Paramètre : tab un tableau d'entiers
    Précondition : tab non vide
    Valeur renvoyée : un entier
    Postcondition : renvoie l'index  du maximum de tab  
    """
    assert len(tab) > 0 #précondition
    #à compléter
    # BEGIN CUT
    imax = 0
    for i in range(1, len(tab)):
        if tab[i] > tab[imax]:
            imax = i
    return imax
    # END CUT

# Tests unitaires
assert indice_maximum([14, 7, 8, 11]) == 0
assert indice_maximum([14, 15, 8, 11]) == 1
assert indice_maximum([14, 7, 18, 11]) == 2
assert indice_maximum([14, 7, 8, 21]) == 3


# ouverture du fichier
f = open('dico.txt',encoding="utf-8")
histo = [0 for _ in range(1000)]
# parcours ligne par ligne
for mot in f:
    v = valeur(mot.rstrip())    
    # à compléter    
    # BEGIN CUT
    if  2000 <= v < 3000:
        histo[v - 2000] = histo[v - 2000] + 1
    # END CUT
# fermeture du fichier
f.close()     
#annee_max = 0 #à modifier
# à compléter
# BEGIN CUT
annee_max =  indice_maximum(histo) + 2000
# END CUT
print(annee_max)
# BEGIN CUT
# Réponse 2160
# END CUT

#%% Exercice 5

def premiere_occurence_naif_enonce(motif, texte):
    """
    Paramètres : motif et texte deux chaînes de caractères
    Précondition :  0 < len(motif) <= len(texte)
    Valeur renvoyée : un entier
    Postcondition : renvoie l'index de la première occurence de motif dans texte
    ou -1 s'il n'y a pas d'occurence
    """    
    p = len(motif)
    n = len(texte)
    assert 0 <= p <= n #précondition 
    for i in range(0, n - p + 1):
        #remplacer le slicing
        if texte[i:i+p] == motif:
            return i
    return -1

def recherche_motif_pos(motif, texte, pos):
    """
    Paramètre : motif et texte deux chaînes de caractères et pos un entier
    Précondition : 0 < len(motif) et  0 <= pos + len(motif) <= len(texte) 
    Valeur renvoyée : un booléen
    Postcondition : détermine si motif == texte[pos:pos+len(motif)] sans slicing
    """
    # précondition
    assert (len(motif) > 0) and  (0 <= pos < len(texte))  
    p = len(motif)
    j = 0
    while j < p and texte[pos + j] == motif[j]:
        j = j + 1
    return j == p

# Tests unitaires
assert recherche_motif_pos("abb", "ababaabba", 5)
assert not recherche_motif_pos("abb", "ababaabba", 2)

#%%
def premiere_occurence_naif(motif, texte):
    """
    Paramètres : motif et texte deux chaînes de caractères
    Précondition :  0 < len(motif) <= len(texte)
    Valeur renvoyée : un entier
    Postcondition : renvoie l'index de la première occurence de motif dans texte
    ou -1 s'il n'y a pas d'occurence
    """    
    p = len(motif)
    n = len(texte)
    assert 0 <= p <= n #précondition 
    for i in range(0, n - p + 1):
        "on remplace le slicing"
        # à compléter
        # BEGIN CUT
        if recherche_motif_pos(motif,texte,i):
            return i
        # END CUT
    return -1

# Tests unitaires
assert premiere_occurence_naif("abb", "ababaabba") == 5
assert premiere_occurence_naif("aba", "ababaabba") == 0
assert premiere_occurence_naif("bba", "ababaabba") == 6
assert premiere_occurence_naif("bbc", "ababaabba") == -1

#%%
# à compléter
# BEGIN CUT
def recherche_motif_pos_trace(motif,texte,pos):
    """
    Paramètre : motif et texte deux chaînes de caractères et pos un entier
    Précondition : 0 <= pos < len(texte) (déjà vérifiée)
    Valeur renvoyée : un  tuple (booléen, entier)
    Postcondition : détermine si motif == texte[i:i+len(motif)] sans slicing 
    et renvoie le nombre de comparaisons effectuées
    """
    #assert 0 <= pos < len(texte)
    c = 0
    for j in range(0, len(motif)):
        c = c + 1
        if motif[j] != texte[pos + j]:
            return (False, c)
    return (True, c)

def premiere_occurence_naif_trace(motif, texte, debug = False):
    n = len(texte)
    p = len(motif)
    assert 0 <= p <= n #précondition
    nb_comparaisons = 0
    for i in range(0, n - p + 1):
        if debug:
            print("Position dans texte : ", i)
            print("Texte : ", texte)
            print("Motif : ", " "*i + motif + " "*(n - (i + p)))
            print('-'*60)
        trouve, c = recherche_motif_pos_trace(motif,texte,i)
        nb_comparaisons += c
        if trouve:      
            return (i, nb_comparaisons)
    return (-1, nb_comparaisons) 

# Test unitaire
assert premiere_occurence_naif_trace('aaabbba','aabaaabaabcaaabbba') == (11, 31)
# END CUT



#%% 

def nombre_occurrence(motif, texte):
    """
    Paramètres : motif et texte deux chaînes de caractères
    Précondition :  0 < len(motif) <= len(texte)
    Valeur renvoyée : un entier
    Postcondition : renvoie le nombre d'occurences de motif dans texte
    """    
    p = len(motif)
    n = len(texte)
    assert 0 < p <= n #précondition 
    # à compléter
    # BEGIN CUT
    c = 0
    for i in range(0, n - p + 1):        
        if recherche_motif_pos(motif,texte,i):
            c = c +1
    # END CUT
    return c

#%% Test sur le texte du "Rouge et le Noir"

f = open("LeRougeEtLeNoir.txt")
texte = f.read()
f.close()
for m in ['Julien', 'Mme de Rênal', 'Mathilde']:
    print(f"Première occurrence du motif  '{m}' dans 'LeRougeEtLeNoir.txt' :", 
    premiere_occurence_naif(m, texte))
    print(f"Nombre d'occurrences du mot '{m}' dans 'LeRougeEtLeNoir.txt' :", 
    nombre_occurrence(m, texte))

# %%
