---
title: TP3
---


* [Énoncé](tp3/I1-TP03-sujetV2.pdf)
* [Archive avec le matériel](tp3/TP3.zip)
* [Corrigé version `.pdf`](tp3/TP3_Correction.pdf)
* [Corrigé version `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/2efc-89027)
* [Corrigé version `.py`](tp3/TP3_Correction.py)

