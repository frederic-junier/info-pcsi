Pour démarrer voici quelques premiers thèmes de TP auxquels je pensais :

* TP1 : révisions sur les types de base , les constructions élémentaires (affectation, expression, conditionnelles, boucles, fonctions), utilisation des modules turtle et random : test de primalité
* TP2: révisions sur le type construit liste vu au lycée (et que nous appellerons tableau), recherche séquentielle dans un tableau (recherche d'élément de maximum), cribles, boucles imbriquées (recherche des deux éléments les plus proches dans un tableau), tri par bulles, introduction à la complexité
* TP3 : spécification de fonctions, précondition, postcondition : assert et manipulation de chaines de chaines de caractères (courbe du dragon, ifs)
* TP4: structures imbriquées : recherche de motif, tri par bulle et introduction à la complexité (tri quadratique), annotations d'un bloc d'instructions par une précondition, une postcondition, une propriété invariante
* TP5: un nouveau type de données construit, le dictionnaire   : comptage (scrabble voir <https://parc-nsi.github.io/premiere/scrabble/>  pour aller plus loin compression byte pair encoding <https://www.codingame.com/ide/puzzle/byte-pair-encoding>), table de symboles pour conversion base 10 <-> 16 (numération maya pour aller plus loin <https://www.codingame.com/training/medium/mayan-calculation>) ou définition des règles pour automate cellulaire à une dimension (https://www.hackinscience.org/exercises/elementary-cellular-automaton), 
* TP6: utilisation de modules, de bibliothèques : analyse d'un fichier de données simples (module csv), calculs statistiques, graphiques avec matplotlib => traitement de données en tables du programme de NSI
* TP7 : Traitement d'images : Algorithmes de rotation (quart de tour voir terminale NSI), de réduction ou d’agrandissement (fade effect voir   <https://introcs.cs.princeton.edu/python/31datatype/index.php> )
Modification d’une image par convolution : flou, détection de
contour, etc

**Décembre => inciter à la participation au concours Prologin**

* TP8 : recherche dichotomique et introduction à la complexité, annotations d'un bloc d'instructions par une précondition, une postcondition, une propriété invariante
* TP9 : récursivité 1 : figures alphanumériques avec print successifs, dessins de fractales, énumération (sous-listes, permutation) recherche dichotomique et exemples d'algos diviser pour régner
* TP10 : réinvestissement des connaissances acquises :  modélisation par simulation probabiliste : étude du problème de percolation  => référence : <https://www.ime.usp.br/~yoshi/2013ii/ccm118/Sedgewick/Slides/24percolation.pdf> 
* TP 11: algorithmes gloutons : Rendu de monnaie. Allocation de salles pour des cours. Sélec-
tion d’activité, Problème du voyageur de commerce voir <https://parc-nsi.github.io/premiere/chapitre24/Glouton.pdf>
* TP 12: tris (sélection, insertion, comptage, fusion ,rapide), annotations d'un bloc d'instructions par une précondition, une postcondition, une propriété invariante
* TP13 : parcours d'un graphe 1/2 : parcours en profondeur à l'aide d'une fonction récursive  ou d'une pile  => détection de composante connexe, application à l'exploration d'un labyrinthe
* TP14: parcours d'un graphe 2/2 :parcours en largeur => distance à l'origine, algorithme de Dijkstraa, heuristique et algorithme A*, détection de graphe bipartis par coloration alternée selon la parité des distances
* TP15: récursivité 2 et graphes : détection de cycle par parcours en profondeur sur un graphe orienté, tri topologique, notice de montage ;  graphes et gloutons : algorithme de Prim 
* TP16: révisions ,  récursivité et tableaux : résolution de sudoku avec retour sur trace

