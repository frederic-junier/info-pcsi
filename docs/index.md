---
title: Accueil
---

Ressources d'informatique commune en PCSI, lycée du Parc, Lyon.

Enseignants :

* Laurent Jouhet
* Stéphane Chenevois
* Frédéric Junier

## Cours

* [Chapitre 1 : constructions de base en Python](cours1.md)
* [Chapitre 2 : tableaux en Python](cours2.md)
* [Chapitre 3 : dictionnaires en Python](cours3.md)
* [Chapitre 4 : chaînes de caractères en Python](cours4.md)

## TP

* [TP1](tp1.md)
* [TP2](tp2.md)
* [TP3](tp3.md)
* [TP4](tp4.md)
* [TP5](tp5.md)
