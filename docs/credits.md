Ce site et ces documents n'existeraient pas sans les sources d'inspiration et les emprunts suivants :

* Code Javascript pour les consoles :
    * Vincent Bouillot : <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>
    * Guillaume Connan : <https://giyom.gitlab.io/ecs1/2020_21/bac_a_sable/>
* Architecture Mkdocs, macros Pythp, :
    * Franck Chambon : <https://ens-fr.gitlab.io/mkdocs/>