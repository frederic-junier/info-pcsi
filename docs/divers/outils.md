---
layout: parc
title:  "Outils et ressources techniques pour le cours de NSI"
---


## Python 

### Installation de Python

- [Installation standard avec Idle](https://www.python.org/downloads/)
- [Installation Anaconda + Pyzo](https://pyzo.org/start.html)
- [Installation Thonny (debugger très pratique)](https://thonny.org/)

### Tutoriels

* Memento / Cheatsheet :
  * [Memento de Laurent Pointal](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
  
### Interpréteurs en ligne 

- [repl.it](https://repl.it/languages/python3)
- [Pythontutor](http://pythontutor.com/)  : permet de visualiser l'évolution des variables
- [basthon (Romain Casati)](https://frederic-junier.org/basthon/) => très bien car on peut téléverser un script, télécharger ou partager en lien et sans créer de compte !
- [basthon version notebook](https://notebook.basthon.fr/) => comme basthon mais pour les notebook jupyter !


### Exerciseurs en ligne :

- [https://www.codepuzzle.io/](https://www.codepuzzle.io/) 

## Éditeurs de textes 

Il est indispensable d'installer un éditeur de textes sur sa plateforme, pour éditer des fichier sources en Python, Javascript ou HTML.

* Un éditeur multiplateformes : [Notepad++](https://notepad-plus-plus.org/)
* Un éditeur pour le développement Web : [Brackets](http://brackets.io/)


