---
layout: parc
title:  "Ressources pour l'informatique commune en PCSI"
---

# Programme

* [Programme](assets/spe774_annexe_1373272_programme_Informatique_Commune.pdf)

# Tutoriels Python

* Un excellent cours pour débuter <https://python.sdv.univ-paris-diderot.fr/>
* Pour aller plus loin le très bon cours du [MOOC Python 3 de la plateforme FUN](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA).


# Plateformes d'entraînement 

* [Pyvert](https://diraison.github.io/Pyvert/) site de Jean Diraison comportant  des exercices avec juge en ligne.


* [France IOI](http://www.france-ioi.org/) : l'inscription est fortement recommandée, nous avons créé un groupe ParcPremiereNSI.

??? video

    <iframe width="300" height="169" sandbox="allow-same-origin allow-scripts allow-popups" src="../assets/presentation-franceioi.mp4" frameborder="0" allowfullscreen></iframe>
    

* Pour s'entraîner sur les constructions élémentaires (boucles, tests, variables, procédures, listes), France IOI propose désormais un parcours en ligne avec le module `turtle`  : <http://www.france-ioi.org/progresser/>

* [Codin Game](https://www.codingame.com) : l'inscription est libre, une orientation plus ludique mais attention ce  site comporte des publicités.

* [Hackinscience](https://www.hackinscience.org)

* Le site de préparation du  concours [Prologin](https://prologin.org/) destiné initialement plutôt à des étudiants en première ou deuxième année postbac, mais c'était avant l'ouverture de la spécialité NSI ... 

[![Prologin](assets/prologin.png "logo prologin")](https://prologin.org/)
    


# Culture informatique


## Langages de programmation 

* Conférence de Judicael Courant : "Une brève histoire des langages de programmation"

??? video
    
    [Captation vidéo de la conférence](https://tube.ac-lyon.fr/videos/watch/2f7065e3-13c7-432c-80cc-94e769d38272)

    <iframe width="300" height="169" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.ac-lyon.fr/videos/embed/2f7065e3-13c7-432c-80cc-94e769d38272" frameborder="0" allowfullscreen></iframe>

* [Conférence de Gérard Berry sur l'importance des langages en informatique (à partie de la minute 12)](https://www.college-de-france.fr/site/gerard-berry/course-2015-11-04-16h00.htm)

## Ordinateur et calcul

* Conférence de Gérard Berry : "Penser, modéliser et maîtriser le calcul informatique"

    [Captation vidéo de la conférence](https://www.college-de-france.fr/site/gerard-berry/inaugural-lecture-2009-11-19-18h00.htm)
    
## Informatique et simulation
    
* [Conférence de Gérard Berry sur la révolution informatique dans les sciences](https://www.college-de-france.fr/site/gerard-berry/course-2015-01-28-16h00.htm)

> "Qu'est-ce que la simulation ? La simulation c'est remplacer l'énergie et la matière par de l'information".




