---
title: Cours 1
---

# Cours 

* [Cours](cours1/I1-Cours-Bases.pdf)


# Ressources


* Boucles `for` sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/Y-Ri2gdiOTA?list=PL2CXLryTKuwwlCGfgpApOTmMn7_dbtwWa" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* Tests et opérateurs booléens sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/0JXc48GXZrU?list=PL2CXLryTKuwwhivE1UO4Jg5DhU-ALAoXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* Boucles `while` sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/gUZFAeM3VCc?list=PL2CXLryTKuwwhivE1UO4Jg5DhU-ALAoXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
