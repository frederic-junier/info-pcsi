#!/usr/bin/env python
# coding: utf-8

# # Faire le point

# # Exercice 1 : décimale <-> binaire

# In[13]:


#%% Exercice 1

from typing import List 

def binaire_vers_decimale(binaire:List[int])->int:
    """
    Renvoie l'écriture décimale d'un entier
    représenté par son tableau de bits en base 2    

    Parameters
    ----------
    binaire : List[int]

    Returns
    -------
    int
    """
    # à compléter

#tests unitaires
assert binaire_vers_decimale([0]) == 0
assert binaire_vers_decimale([1]) == 1
assert binaire_vers_decimale([1, 0]) == 2
assert binaire_vers_decimale([1, 1]) == 3

#%% Exercice 1

def est_binaire(tab:List[int])->bool:
    """
    Renvoie True si tab est un tableau non vide
    contenant uniquement des 0 ou des 1

    Parameters
    ----------
    tab : List[int]
        Précondition : len(tab) > 0

    Returns
    -------
    bool
    """
    assert len(tab) > 0 #précondition
    #à compléter

#tests unitaires pour est_binaire
assert est_binaire([0])
assert not est_binaire([0,4])
assert not est_binaire([4, 1])
assert est_binaire([1,0,0])

#%% Exercice 1

def decimale_vers_binaire(n:int)->List[int]:
    """
    Renvoie un tableau de bits (0 ou 1)
    représentant l'écriture binaire de l'entier n en base 10

    Parameters
    ----------
    n : int    
        Précondition : n >= 0
        
    Returns
    -------
    List[int]
    """    
    assert n >= 0  #précondition 
    tmp = n
    t = []
    while n >= 2:
        assert n - 2 >= 0 
        assert tmp == n + binaire_vers_decimale(t) 
        # à compléter
    # à compléter
    return t

#tests unitaires à compléter


# # Dictionnaires en Python

# ## Exercice 2
# 
# <https://fr.wikipedia.org/wiki/Les_Shadoks>

# In[25]:


#%% Exercice 2

from typing import List, Dict

def nb_chiffres_shadok(n:int)->int:
    """
    Renvoie le nombre de chiffres de n en base 4
    
    Parameters
    ----------
    n : int
        Précondition  n >= 0

    Returns
    -------
    int
    """
    assert n >= 0 #Précondition
    #à compléter
    
    
#Tests unitaires
assert nb_chiffres_shadok(0) == 1
assert nb_chiffres_shadok(1) == 1
    
#%% Exercice 2
def decimale_vers_shadok(n:int)->List[str]:
    """
    Renvoie la représentation en base 4 de l'entier n
    en base 10 avec les chiffres Shadoks 'G', 'B', 'Z', 'M'

    Parameters
    ----------
    n : int
        Précondition  n >= 0

    Returns
    -------
    List[str]
    """
    assert n >= 0 #Précondition
    tab_decimal_shadok = ['G','B','Z','M']
    chiffres = ['G'] * nb_chiffres_shadok(n)
    i = len(chiffres) - 1
    while i >= 0:
        #à compléter
        i = i - 1
    return chiffres

#tests unitaires
assert [decimale_vers_shadok(k) for k in range(8)] == [['G'], ['B'], ['Z'], ['M'], ['B', 'G'], ['B', 'B'], ['B', 'Z'], ['B', 'M']]

#%% Exercice 2
def est_shadok(tab:List[str])->bool:
    """
    Renvoie True ssi tab, non vide, est une représentation shadok
    d'un entier positif
    avec la table de symboles {'G':0, 'B':1, 'Z':2, 'M':3}

    Parameters
    ----------
    tab : List[str]
        
    Returns
    -------
    bool
    """
    if len(tab) == 0:
        return False
    #à compléter
    
#tests unitaires
assert est_shadok(['G']) 
assert est_shadok(['Z', 'G']) 
assert not est_shadok(['G', 'B'])  #pas de zéro en chiffre 1
assert not est_shadok(['z', 'G'])   #sensible à la casse

#%% Exercice 2
def shadok_vers_decimale(shadok:List[str])->int:
    """
    Renvoie l'écriture décimale de l'entier 
    donné par son tableau de  représentation en base 4 shadok

    Parameters
    ----------
    shadok : List[str]
        Précondition : est_shadok(shadok)

    Returns
    -------
    int
    """
    assert est_shadok(shadok)
    dico_shadok_decimal = {'G':0, 'B':1, 'Z':2, 'M':3}
    #à compléter
    
#tests unitaires
assert shadok_vers_decimale(['G']) == 0 
assert shadok_vers_decimale(['B']) == 1 
assert shadok_vers_decimale(['Z']) == 2 
assert shadok_vers_decimale(['M']) == 3
assert shadok_vers_decimale(['B', 'G']) == 4 
assert shadok_vers_decimale(['B', 'B']) == 5
assert shadok_vers_decimale(['B', 'Z']) == 6
assert shadok_vers_decimale(['B', 'M']) == 7
assert shadok_vers_decimale(['B', 'G', 'G']) == 16
#plus court avec la fonction all
assert all([shadok_vers_decimale(decimale_vers_shadok(n)) == n for n in range(17)])


# ## Exercice 3 Table des  caractères

# In[30]:


#%% Exercice 3
#à compléter


# In[31]:


#à compléter


# ##  Exercice 4 Table des symboles

# In[13]:


#%% Exercice 4 Partie 1

from typing import List

def affiche_symbole(table:dict, espace:str, list_symbol:List[str])->None:
    for symbol in list_symbol:
        print(symbol, "dans table ", espace, " ?")
        try:            
            print(symbol, ":", table[symbol])
        except KeyError as error:
            print("KeyError : ", error)


if "a" in globals():
    del globals()["a"]
affiche_symbole(globals(), "globale", ['a'])
print("-"*80)
a = 1
print("a =",a)
affiche_symbole(globals(), "globale", ['a'])
print("-"*80)

def f():
    a = 842
    print("a=",a)
    affiche_symbole(globals(), "globale", ['a'])
    affiche_symbole(locals(), "globale", ['a'])
    print("-"*80)
    
f()
print("a=",a)
affiche_symbole(globals(), "globale", ['a'])


# In[11]:


#%% Exercice 4 Partie 2

from typing import List

def affiche_symbole(table:dict, espace:str, list_symbol:List[str])->None:
    for symbol in list_symbol:
        print(symbol, "dans table ", espace, " ?")
        try:            
            print(symbol, ":", table[symbol])
        except KeyError as error:
            print("KeyError : ", error)


a, b, c = 731, 734, 735
print("a =",a,",","b =",b,",", "c =",c)
affiche_symbole(globals(), "globale", ['a','b','c'])
print("-"*80)

def f():
    b, c = 736, 737    
    print("a =",a,",","b =",b,",", "c =",c)
    affiche_symbole(globals(), "globale", ['a','b','c'])
    affiche_symbole(locals(), "locale", ['a','b','c'])
    print("-"*80)
    def g():
        c = 738
        print("a =",a,",","b =",b,",", "c =",c)
        affiche_symbole(globals(), "globale", ['a','b','c'])
        affiche_symbole(locals(), "locale", ['a','b','c'])
        print("-"*80)
    g()
    print("a =",a,",","b =",b,",", "c =",c)
    affiche_symbole(globals(), "globale", ['a','b','c'])
    affiche_symbole(locals(), "locale", ['a','b','c']) 
    print("-"*80)
    
f()
print("a =",a,",","b =",b,",", "c =",c)
affiche_symbole(globals(), "globale", ['a','b','c'])


# ## Exercice 5 : dictionnaire look up

# In[32]:


#%% Exercice 5

from typing import List, Dict

def csv_vers_dico(chemin:str, separateur:str, index_clef:int, index_valeur:int)->Dict[str,str]:
    """
    docstring à compléter
    """
    dico = {}  #ou dico  = dict()
    f = open(chemin)
    for ligne in f:
        champs = ligne.rstrip().split(separateur)
        dico[champs[index_clef]]= champs[index_valeur]
    f.close()
    return dico


# In[36]:


#%% Exercice 5
#Question 2 à compléter
#tests unitaires
assert len(dico_codons_acides) == 64
assert dico_codons_acides['TTA'] == 'Leucine'
assert dico_codons_acides['TGG'] == 'Tryptophan'


# In[37]:


#%% Exercice 5

from typing import List, Dict

def inversion_dico(dico:Dict[str,str])->Dict[str,List[str]]:
    """
    Renvoie le dictionnaire inversé (valeur, clef)
    à partir dico dictionnaire (clef, valeur)

    Parameters
    ----------
    dico : Dict[str,str]

    Returns
    -------
    Dict[str,List[str]]
    """
    ocid = {}
    #à compléter
    return ocid

#tests unitaires
dico_codons_acides = csv_vers_dico("amino.csv", ",", 0, 3)
dico_acides_codons = inversion_dico(dico_codons_acides)
assert dico_acides_codons['Leucine'] == ['TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG']
assert dico_acides_codons['Tryptophan'] == ['TGG']
assert len(dico_acides_codons) == 20


# ## Exercice 6 Recherche de doublons

# In[39]:


#%% Exercice 6

from typing import List, Dict

def recherche_doublons(tab:List[int])->bool:
    """
    Renvoie un booléen indiquant
    si tab contient au moins un doublon

    Parameters
    ----------
    tab : List[int]
        Précondition : len(tab) > 0

    Returns
    -------
    bool
    """
    assert len(tab) > 0 #précondition
    #à compléter
    
    
#tests unitaires
assert not recherche_doublons([1,2,3])
assert recherche_doublons([1,2,2])
assert not recherche_doublons([1])
assert recherche_doublons([1, 1])
assert recherche_doublons([2, 1, 2])
assert recherche_doublons([2, 2, 1])


#%% Exercice 6

def recherche_doublons2(tab:List[int])->bool:
    """
    Renvoie un booléen indiquant
    si tab contient au moins un doublon

    Parameters
    ----------
    tab : List[int]
        Précondition : len(tab) > 0

    Returns
    -------
    bool
    """
    assert len(tab) > 0 #précondition
    #à compléter
    
def recherche_doublons_comptage(tab:List[int])->bool:
    """
    Renvoie un booléen indiquant
    si tab contient au moins un doublon

    Parameters
    ----------
    tab : List[int]
        Précondition : len(tab) > 0

    Returns
    -------
    bool
    """
    assert len(tab) > 0 #précondition    
    histo = {}
    #à compléter


# In[31]:


#%% Exercice 6
from typing import Callable, List, Tuple,  Any
import time

def pire_cas(taille):
    return list(range(taille))


def test_doublement_ratio_pire_cas(nb_iter:int, fonction:Callable[[List[int]],Any], debug = True)->Tuple[List[int], List[float], List[float]]:
    """
    Paramètres : nb_iter un entier, fonction une fonction de paramètre un tableau d'entiers
    et renvoyant n'importe quel type, debug un booléen à True par défaut
    Valeur renvoyée : un tableau d'entiers, deux tableaux de flottants
    Postcondition : exécute fonction sur des tableaux list(range(taille)) pour une taille doublant nb_iter fois
    à partir de taille = 100 et renvoie les tableaux de taille, de temps d'exécution, de rapport entre des temps
    d'exécutions pour des tailles successives dans un rapport de 2
    """
    def mesurer_pire_temps(taille):
        tab =  pire_cas(taille)
        debut = time.perf_counter()
        fonction(tab)
        return time.perf_counter() - debut
    
    taille = 100
    tab_taille = []
    tab_temps = []
    tab_ratio = []    
    temps = mesurer_pire_temps(taille)
    tab_temps.append(temps)
    tab_taille.append(taille)
    if debug:
            print(f"Taille = {taille} --> Temps = {temps:.3e} s")  
    for k in range(nb_iter - 1):
        taille = taille * 2 
        temps = mesurer_pire_temps(taille)
        tab_temps.append(temps)
        tab_taille.append(taille)
        ratio = temps/tab_temps[k]
        tab_ratio.append(ratio)
        if debug:
            print(f"Taille = {taille} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")             
    return tab_taille, tab_temps, tab_ratio


# In[35]:


test_doublement_ratio_pire_cas(8, recherche_doublons)


# In[47]:


test_doublement_ratio_pire_cas(8, recherche_doublons_comptage)


# ## Exercice 7 : recherche de deux valeurs les plus proches dans un tableau

# In[47]:


#%% Exercice 7

from typing import List, Tuple

def deux_plus_proches(tab:List[int])->Tuple[int, int]:
    """
    Renvoie un les index des deux valeurs les plus proches
    du tableau d'entiers tab, dans l'ordre croissant

    Parameters
    ----------
    tab : List[int]
        Précondition : len(tab) > 1

    Returns
    -------
    Tuple[int, int]
    """
    
    assert len(tab) > 1   #précondition    
    dmin = float('inf')   #distance min
    
#tests unitaires
assert deux_plus_proches([1,0,2]) == (0,1)
assert deux_plus_proches([1,1,1]) == (0,1)
assert deux_plus_proches([1, 5, 4]) == (1, 2)
assert deux_plus_proches([1, 4, 5]) == (1, 2)


# In[48]:


#%% Exercice 7

from typing import List, Dict, Tuple, Callable
from random import randint
import time

def tableau_aleatoire(binf:int, bsup:int, taille:int)->List[int]:
    """
    Renvoie un tableau d'entiers aléatoires entre binf et bsup
    de longueur taille

    Parameters
    ----------
    binf : int        
    bsup : int
    taille : int
  
    Returns
    -------
    List[int]
    """
    #précondition à compléter
    return [randint(binf, bsup) for _ in range(taille)]


#%% Exercice 7

def test_doublement_ratio_cas_moyen(nb_iter:int, fonction:Callable[[List[int]],Any], taille_echantillon:int, debug = True)->Tuple[List[int], List[float], List[float]]:
    """
    Paramètres : nb_iter un entier, fonction une fonction de paramètre un tableau d'entiers
    et renvoyant n'importe quel type, debug un booléen à True par défaut
    Valeur renvoyée : un tableau d'entiers, deux tableaux de flottants
    Postcondition : exécute fonction sur des tableaux list(range(taille)) pour une taille doublant nb_iter fois
    à partir de taille = 100 et renvoie les tableaux de taille, de temps d'exécution, de rapport entre des temps
    d'exécutions pour des tailles successives dans un rapport de 2
    """

    
    def mesurer_temps_moyen(taille):
        s = 0
        for _ in range(taille_echantillon):
            tab =  tableau_aleatoire(-taille, taille, taille)
            debut = time.perf_counter()
            fonction(tab)
            s = s + time.perf_counter() - debut
        return s / taille_echantillon
    
    taille = 100
    tab_taille = []
    tab_temps = []
    tab_ratio = []    
    temps = mesurer_temps_moyen(taille)
    tab_temps.append(temps)
    tab_taille.append(taille)
    if debug:
            print(f"Taille = {taille} --> Temps = {temps:.3e} s")  
    for k in range(nb_iter - 1):
        taille = taille * 2 
        temps = mesurer_temps_moyen(taille)
        tab_temps.append(temps)
        tab_taille.append(taille)
        ratio = temps/tab_temps[k]
        tab_ratio.append(ratio)
        if debug:
            print(f"Taille = {taille} --> Temps = {temps:.3e} s --> Ratio = {ratio:.3e}")             
    return tab_taille, tab_temps, tab_ratio


# In[100]:


test_doublement_ratio_cas_moyen(6, deux_plus_proches, 20)


# * __Observation expérimentale :__ 
# On observe que dans le pire des cas,  le temps d'exécution quadruple lorsque la  la taille de l'échantillon est doublée, on peut donc conjecturer que la __complexité temporelle__ de l'algorithme est en $O(n^{2})$. 
# * __Analyse de l'algorithme dans le pire des cas :__   
# Si on compte les comparaisons et affectations, comme on a des boucles imbriquées, l'ordre de grandeur de la __complexité temporelle__ est fixée par l'exécution de la __boucle interne__. On a deux boucles imbriquées  avec au plus $n$ itérations pour chacune dans le __pire des cas__, cela nous donne un nombre d'opérations élémentaires inférieur à  $kn^2$ donc une  __complexité temporelle__ en $O(n^{2})$. Plus précisément, on a dans le __pire des cas__, $\sum_{k=1}^{n-1}(n-k)=\frac{(n-1)n}{2}$ exécutions de la boucle interne. 

# In[51]:


#%% Exercice 7

def permuter(tab:List[int], i:int, j:int)->None:
    """Paramètres :tab un tableau d'entiers,  i et j des entiers
    Précondition : tab non vide, i et j index valides
    Valeur renvoyée : aucun (None par défaut)
   	Postcondition :  permute les valeurs tab[i]  et tab[j]"""
    n = len(tab) 
    assert n > 0 and 0 <= i < n and 0 <= j < n  #précondition
    tab[i], tab[j] = tab[j], tab[i]
    
def tri_bulle1(tab:List[int])->None:
    n = len(tab)
    for i in range(n):
        for j in range(n - 1):
            if tab[j] > tab[j + 1]:
                permuter(tab, j, j + 1)  


# In[53]:


test_doublement_ratio_cas_moyen(6, tri_bulle1, 20)


# * __Observation expérimentale :__ 
# On observe que dans le pire des cas,  le temps d'exécution quadruple lorsque la  la taille de l'échantillon est doublée, on peut donc conjecturer que la __complexité temporelle__ de l'algorithme est en $O(n^{2})$. 
# * __Analyse de l'algorithme :__   
# Si on compte les comparaisons et affectations, comme on a deux boucles imbriquées avec au plus $n$ itérations cela nous donne un nombre d'opérations élémentaires en $kn^2$ donc  __complexité temporelle__ en $O(n^{2})$.

# ## Exercice 8 : dictionnaire et  Scrabble
# 
# <https://www.codingame.com/ide/puzzle/scrabble>

# In[ ]:


#%% Exercice 8

#dictionnaire des valeurs des lettres minuscules au scrabble
valeur = {
    'a' : 1,
    'e' : 1,
    'i' : 1,
    'o' : 1,
    'n' : 1,
    'r' : 1,
    't' : 1,
    'l' : 1,
    's' : 1,
    'u' : 1,
    'd' : 2,
    'g' : 2,
    'b' : 3, 'c' : 3, 'm' : 3, 'p' : 3,
    'f' : 4, 'h' : 4, 'v' : 4, 'w' : 4, 'y' : 4,
    'k' : 5, 
    'j' : 8, 'x' : 8,
    'q' : 10, 'z' : 10}

def signature(mot):
    """Paramètre : mot de type str
    Valeur renvoyée : un dictionnaire représentant le nombre d'occurences
    de chaque lettre minuscule de l'alphabet dans mot""" 
    sig  = {}  #dictionnaire vide
    for c in mot:
        "à compléter"
    return sig

#Tests unitaires de la fonction signature
assert signature("") == {}
assert signature("ananas") == {'a': 3, 'n': 2, 's': 1}
assert signature("abcd") == {'a': 1, 'b': 1, 'c': 1, 'd': 1}


def score_mot(mot, valeur):
    """Paramètre : mot de type str, 
    valeur de type dict associe à chaque lettre minuscule sa valeur au scrabble
    Valeur renvoyée : score du mot au scrabble""" 
    s = 0
    "à compléter"
    return s

#Tests unitaires de la fonction signature
assert score_mot("") == 0
assert score_mot("zazou") == 23
assert score_mot("ananas") == 6


def mot_possible(mot, sig_lettres):
    """Paramètre : mot de type str
    sig_lettres de type dict représente la signature des lettres disponibles
    Valeur renvoyée : un booléen indiquant si sig_mot compatible avec  sig_lettres
    et donc si le mot peut être composé avec les lettres fournies"""
    sig_mot = signature(mot)
    "à compléter"


#Tests unitaires de la fonction mot_possible
assert mot_possible("zazou", {"a" : 1, "b" : 1, "o" : 2, "u" : 1, "z" : 2}) == True
assert mot_possible("zazou", {"b" : 1, "o" : 1, "u" : 1, "z" : 2}) == False
assert mot_possible("zazou", {"a" : 1, "o" : 2, "u" : 3, "z" : 1}) == False

#Programme principal
# saisie du nombre de mots du dictionnaire
n = int(input())
# saisie des mots du dictionnaire dans une liste
dico = [input().rstrip() for _ in range(n)]
letters = input().rstrip()
sig_lettres = signature(letters)
smax = -1
mot_max = ""
# boucle sur les mots du dictionnaire
for mot in dico:
    "à compléter"


#soumettre sur https://www.codingame.com/ide/puzzle/scrabble
print(mot_max)


# ## Exercice 9 : automate cellulaire élémentaire
# 
# Sources :
# * <https://en.wikipedia.org/wiki/Elementary_cellular_automaton>
# * <https://www.hackinscience.org/exercises/elementary-cellular-automaton>

# In[60]:


#%% Exercice 9

from typing import Dict, List

def decimale_vers_binaire_str(n:int, bit_max:int)->str:
    """
    Renvoie la représentation de l'entier n en base deux
    sur bit_max bits  en remplissant éventuellement par
    des '0' à gauche

    Parameters
    ----------
    n : int   précondition n >= 0
    bit_max : int     précondition bit_max  > 0

    Returns
    -------
    str
    """
    assert n >= 0 and bit_max > 0
    #à compléter
    binaire = ''.join([str(bit) for bit in decimale_vers_binaire(n)])
    return '0'*(bit_max - len(binaire)) + binaire
    
    
#tests unitaires à compléter

#%% Exercice 9
def transition(regle:int)->Dict[str,int]:
    """
    Renvoie une dictionnaire de de clefs str et valeurs int
    représente la fonction de transition
    pour l'automate celullaire élémentaire associé à regle
    
    Parameters
    ----------
    regle : int
           Précondition : 0 <= regle <= 255
    Returns
    -------
    Dict[str,int]
 
    """    
    assert 0 <= regle <= 255


#tests unitaires
assert transition(12) == {'111': '0', '110': '0',  '101': '0',  '100': '0',  '011': '1', '010': '1', '001': '0',
 '000': '0'}
assert transition(110) == {'111': '0', '110': '1', '101': '1', '100': '0', '011': '1', '010': '1', '001': '1',
 '000': '0'}


# In[62]:


#%% Exercice 9
def automate(regle:int,nb_iter:int)->None:
    """
    Affiche les nb_iter lignes produites par l'automate cellulaire
    élémentaire donné par sa regle.
    La première ligne compte 79 caractères : '0'*39 + '1' + '0'*39
    """
    ligne = '0'*39 + '1' + '0'*39
    print(ligne)


# In[65]:


automate(90, 20)

