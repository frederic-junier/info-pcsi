#!/usr/bin/env python
# coding: utf-8

## Faire le point

#%% Exercice 1


def somme_puissance(a:int, b:int, n:int)->int:
    """Renvoie a**n + (a+1) ** n + .... + b ** n"""
    s = 0
    for k in range(a, b + 1):
        s = s + k ** n
    return s

#Tests unitaires sur les sommes d'Euler
assert somme_puissance(1, 100, 1) == (100 * (100 + 1)) // 2
assert somme_puissance(1, 100, 2) == (100 * (100 + 1) * (2*100 + 1)) // 6
assert somme_puissance(1, 100, 3) == ((100 * (100 + 1) ) // 2) ** 2


def somme_puissance(a:int, b:int, n:int)->int:
    """Renvoie a**n + (a+1) ** n + .... + b ** n"""
    #précondition
    assert a <= b,"précondition a <= b"
    s = 0
    for k in range(a, b + 1):
        s = s + k ** n
    return s

#Tests unitaires sur les sommes d'Euler
assert somme_puissance(1, 100, 1) == (100 * (100 + 1)) // 2
assert somme_puissance(1, 100, 2) == (100 * (100 + 1) * (2*100 + 1)) // 6
assert somme_puissance(1, 100, 3) == ((100 * (100 + 1) ) // 2) ** 2



#%% Exercice 2

def syracuse(n:int)->int:
    """
    Paramètre : n de type int
    Valeur renvoyée : de type int
    Précondition : n > 0
    Postcondition: renvoie le terme suivant n dans la suite de Syracuse    
    """
    assert n > 0
    if n%2 == 0:
        return n//2
    else:
        return 3*n + 1
    
#Tests unitaires
assert syracuse(1) == 4
assert syracuse(4) == 2



def duree_vol(n:int)->int:
    """
    Paramètre : n de type int
    Valeur renvoyée : de type int
    Précondition : n > 0
    Postcondition: renvoie la durée de vol de la suite de Syracuse
    de premier terme n
    """
    assert n > 0
    t = 0
    while n != 1:
        n = syracuse(n)
        t = t + 1
    return t
    
#Tests unitaires
assert duree_vol(4) == 2
assert duree_vol(842) == 41


def altitude(n:int)->int:
    """
    Paramètre : n de type int
    Valeur renvoyée : de type int
    Précondition : n > 0
    Postcondition: renvoie la valeur maximale
    atteinte par la suite de Syracuse de premier terme n
    avant d'atteindre 1
    """
    assert n > 0
    altmax = n
    while n != 1:
        n = syracuse(n)
        if n > altmax:
            altmax = n
    return altmax
    
#Tests unitaires
assert altitude(4) == 4
assert altitude(842) == 1264


## Opérations élémentaires sur les tableaux

#%% Exercice 3

# Créer le tableau tab1 = [842,841,833]

tab1 = [842, 841, 843]


# Remplacer la valeur tab1[2] par 843


tab1[2] = 843


# Échanger les valeurs tab1[0] et tab[1] en une seule affectation grace au sequence unpacking.

tab1[0], tab1[1] = tab1[1], tab1[0]



print(tab1)


# Ajouter l’élément 941 à tab1 par concaténation.



tab1 = tab1 + [941]


# Ajouter l’élément 941 à tab1 avec la méthode `append`.


tab1 = [842, 841, 843]
tab1.append(941)
print(tab1)


# Concaténer tab1 et le tableau tab2 = [942, 943]

tab2 = [942, 943]
tab4 = tab1 + tab2
print(tab4)


# Créer un tableau tab3 contenant cent fois 0.

# __Méthode 1:__  par concaténations successives

tab31 = []
for _ in range(100):
    tab31 = tab31  + [0]


# Ou de façon raccourcie
tab32 = [0] * 100


# __Méthode 2:__ par compréhension de listes

tab33 = [0 for _ in range(100)]

#test 
tab31 == tab32 and tab32 == tab33


#%% Exercice 4 : tableau en compréhension

# Un tableau de dix lancers d’un dé équilibré à 6 faces numérotées de 1 à 6. On utilisera la fonction
# randint du module random.

# In[49]:


from random import randint
echantillon = [randint(1,6) for _ in range(10)]
print(echantillon)


# Le tableau des carrés des entiers compris entre 1 et 20.

t2 = [k**2 for k in range(1, 21)]
print(t2)


# Le tableau des entiers impairs compris entre 1 et 20.

t3 = [k for k in range(1, 21, 2)]
print(t3)


# Le tableau des carrés des entiers impairs compris entre 1 et 20.

t4 = [k**2 for k in range(1, 21, 2)]
print(t4)


# Le tableau des entiers compris entre 0 et 100 qui sont multiples de 3 ou de 5.

t5 = [k for k in range(0, 101) if k%3 == 0 or k%5==0]


# Le tableau des entiers pairs compris entre 20 et 0 pris dans l’ordre décroissant. Écrire le même
# tableau avec list et range .

t6 = [k for k in range(20, -1, -2)]
print(t6)

t7 = list(range(20, -1, -2))


t7 == t6

#%% Exercice 5 Slicing, découpage en tranches

t1 = list(range(10, 1, -1))
t2 = t1[1:3]
t3 = t1[2:3]

# Une copie superficielle, ou `shallow copy`
t4 = t1[:]
t5 = t1[::2]
t6 = t1[2:5:2]
t7 = t1[::-1]
t8 = t1[6:2:-1]

#%% Exercice 6
from typing import List

def vsom(tab1:List[int], tab2:List[int])->List[int]:
    """
    Paramètres :
        tab1 et tab2 des tableaux de type list contenant des  entiers de type int
    Préconditions :
        tab1 non vide et len(tab1) == len(tab2)
    Valeur renvoyée :
        un tableau de type list contenant des  nombres de type int
    Postcondition :
        le tableau renvoyé est constitué des sommes
        terme à terme des éléments de tab1 et tab2        
    """
    assert len(tab1) > 0 and len(tab1) == len(tab2), "tab1 et tab2 doivent être de même longueur"
    return [tab1[k] + tab2[k] for k in range(len(tab1))]

#tests unitaires
assert vsom([1, 2, 3], [4, 5, 6]) == [5, 7, 9]


def smul(c:int, tab1:List[int])->List[int]:
    """
    Paramètres :
        tab1 tableau d'entiers et c entier
    Préconditions :
        tab1 non vide
    Valeur renvoyée :
        un tableau de type list contenant des  nombres de type int
    Postcondition :
        le tableau renvoyé est constitué des produits
        de chaque élément de tab1 par c       
    """
    assert len(tab1) > 0 
    return [e * c for e in tab1]

#tests unitaires
assert smul(2, [1, 2, 3])  == [2, 4, 6]


def vdif(tab1:List[int], tab2:List[int])->List[int]:
    """
    Paramètres :
        tab1 et tab2 des tableaux de type list contenant des  entiers de type int
    Préconditions :
        tab1 non vide et len(tab1) == len(tab2)
    Valeur renvoyée :
        un tableau de type list contenant des  nombres de type int
    Postcondition :
        le tableau renvoyé est constitué des différences
        terme à terme des éléments de tab1 et tab2        
    """
    assert len(tab1) > 0 and len(tab1) == len(tab2), "tab1 et tab2 doivent être de même longueur"
    return [tab2[k] - tab1[k] for k in range(len(tab1))]


#tests unitaires
assert vdif([1, 2, 3], [4, 5, 6]) == [3, 3, 3]


def vprod(tab1:List[int], tab2:List[int])->List[int]:
    """
    Paramètres :
        tab1 et tab2 des tableaux de type list contenant des  entiers de type int
    Préconditions :
        tab1 non vide et len(tab1) == len(tab2)
    Valeur renvoyée :
        un tableau de type list contenant des  nombres de type int
    Postcondition :
        le tableau renvoyé est constitué des produits
        terme à terme des éléments de tab1 et tab2        
    """
    assert len(tab1) > 0 and len(tab1) == len(tab2), "tab1 et tab2 doivent être de même longueur"
    return [tab2[k] * tab1[k] for k in range(len(tab1))]


#tests unitaires
assert vprod([1, 2, 3], [4, 5, 6]) == [4, 10, 18]


#%% Exercice 7

#parcours par index
def somme_tab(tab):
    """
    Paramètres :tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée : un entier de type int
    Postcondition : renvoie la somme des éléments de tab
    """
    assert len(tab) > 0, "tab doit être non vide"    
    somme = 0
    for k in range(len(tab)):
        "à compléter"
        #BEGIN CUT
        somme = somme + tab[k]
        #END CUT
    return somme


#parcours par valeur/élément
def somme_tab2(tab):
    """
    Paramètres :tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée : un entier de type int
    Postcondition : renvoie la somme des éléments de tab
    """
    assert len(tab) > 0, "tab doit être non vide"
    somme = 0
    for element in tab:
        "à compléter"
        #BEGIN CUT
        somme = somme + element
        #END CUT
    return somme
    
#Tests unitaires
assert somme_tab([10]) == 10
assert somme_tab([10, 2]) == 12
assert somme_tab([-1, 1]) == 0
assert somme_tab([-1, -2,-3]) == -6

#%%  Exercice 8

from typing import List

def appartient1(val:int,tab:List[int])->bool:
    """
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un booléen
    Postcondition : renvoie la valeur de  val in tab
    """
    assert len(tab) > 0
    i = 0
    trouve = False
    while i < len(tab):
        #variant de boucle  len(tab) - i
        #à compléter
        #BEGIN CUT
        if tab[i] == val:
            trouve = True
        #END CUT
        i = i  + 1
    #sortie de boucle :  i == len(tab)  et len(tab) - i == 0
    return trouve

#Tests unitaires
assert appartient1(1, [1,1,1,1])
assert appartient1(1, [1,0,0,0])
assert not appartient1(1, [0,0,0,0])
assert appartient1(1, [0,0,0,1])
assert not appartient1(1, [0,0,0,0])
assert appartient1(1, [0,0,1,1])


def appartient2(val:int,tab:List[int])->bool:
    """
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un booléen
    Postcondition : renvoie la valeur de  val in tab
    """
    assert len(tab) > 0
    i = 0
    #à compléter avec une boucle while 
    #BEGIN CUT
    while i < len(tab) and tab[i] != val:
        #variant de boucle  len(tab) - i
        i = i  + 1
    #END CUT
    #sortie de boucle :  i == len(tab)  ou tab[i] == val (sortie prématurée)
    return i < len(tab)

#Tests unitaires
assert appartient2(1, [1,1,1,1])
assert appartient2(1, [1,0,0,0])
assert not appartient2(1, [0,0,0,0])
assert appartient2(1, [0,0,0,1])
assert not appartient2(1, [0,0,0,0])
assert appartient2(1, [0,0,1,1])

def appartient3(val:int,tab:List[int])->bool:
    """
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un booléen
    Postcondition : renvoie la valeur de  val in tab
    """
    assert len(tab) > 0
    #pas besoin d'invariant, terminaison assurée boucle bornéee
    for i in range(len(tab)):
        "à compléter avec une sortie prématurée"
        #BEGIN CUT
        if tab[i] == val:
            return True
        #END CUT
    #à compléter
    #BEGIN CUT
    return  False
    #END CUT

#Tests unitaires
assert appartient3(1, [1,1,1,1])
assert appartient3(1, [1,0,0,0])
assert not appartient3(1, [0,0,0,0])
assert appartient3(1, [0,0,0,1])
assert not appartient3(1, [0,0,0,0])
assert appartient3(1, [0,0,1,1])


def index(val:int,tab:List[int])->int:
    """
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie l'index de la première occurrence de val dans tab et len(tab) si val not in tab
    """
    assert len(tab) > 0
    #à compléter
    for i in range(len(tab)):
        if tab[i] == val:
            return i
    return len(tab)

#Tests unitaires
assert index(1, [1,1,1,1]) == 0
assert index(1, [0,2,3]) == 3
assert index(1, [1,0,0,0]) == 0
assert index(1, [0,0,1]) == 2


## Recherche linéaire et correction : invariant de boucle

## Exemple

def puissance(x:float, n:int)->float:
    """Paramètres : x un réel et n un entier
    Précondition : n >= 0
    Valeur renvoyé: un réel
    Postcondition: renvoie x ** n sans utiliser l'exponentiation"""
    assert n >= 0
    p = 1
    for i in range(0, n):
        #invariant : p == x ** i
        p = p * x
    return p


#%%  Exercice 9 : recherche linéaire de maximum

# Solution avec parcours par index


from typing import List


def valeur_maximum(tab:List[int])->int:
    """
    Paramètres : tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie la valeur de max(tab)
    """
    #précondition
    assert len(tab) > 0
    maxi = tab[0] #notez l'initialisation 
    #à compléter
    #BEGIN CUT
    for k in range(1, len(tab)):
        #invariant : maxi = max(tab[0:k])
        if tab[k] > maxi:
            maxi = tab[k]
    return maxi
    #END CUT

#Tests unitaires
assert valeur_maximum([1,1,1,1]) == 1
assert valeur_maximum([-1]) == -1
assert valeur_maximum([3,2,1]) == 3
assert valeur_maximum([1,2,3]) == 3
assert valeur_maximum([-3,-2,-1]) == -1
assert valeur_maximum([-3,2,-1]) == 2


# Solution avec parcours par valeur

def valeur_maximum2(tab):
    """
    Paramètres : tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie la valeur de max(tab)
    """
    #précondition
    assert len(tab) > 0
    maxi = -float('inf') #notez l'initialisation 
    #à compléter
    #BEGIN CUT
    for element in tab:
        #invariant : maxi = max(tab[0:k])
        if element > maxi:
            maxi = element
    return maxi
    #END CUT

#Tests unitaires
assert valeur_maximum2([1,1,1,1]) == 1
assert valeur_maximum2([-1]) == -1
assert valeur_maximum2([3,2,1]) == 3
assert valeur_maximum2([1,2,3]) == 3
assert valeur_maximum2([-3,-2,-1]) == -1
assert valeur_maximum2([-3,2,-1]) == 2

#%% Recherche de l'index du maximum
def index_maximum(tab:List[int])->int:
    """
    Paramètres :  tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie l'index de la première occurence du maximum de tab
    """
    #précondition
    assert len(tab) > 0
    #à compléter
    #BEGIN CUT
    maxi = tab[0]
    imax = 0
    for k in range(1, len(tab)):
        #invariant : maxi = max(tab[0:k]) et imaxi = min([i for i range(0, k) if tab[i] == maxi])
        if tab[k] > maxi:
            maxi = tab[k]
            imax = k
    return imax
    #END CUT

#Tests unitaires
assert index_maximum([1,1,1,1]) == 0
assert index_maximum([-1]) == 0
assert index_maximum([3,2,1]) == 0
assert index_maximum([1,2,3]) == 2
assert index_maximum([-3,-2,-1]) == 2
assert index_maximum([-3,2,-1]) == 1
    
#%% Deux plus grands

from typing import List

def deux_plus_grands(tab:List[int])->List[int]:
    """
    Paramètres :  tab tableau  d'entiers de type int
    Précondition : tab de taille >= 2
    Valeur renvoyée :  un tableau de deux entiers
    Postcondition : renvoie les deux plus gros éléments de tab
    """
    #précondition
    assert len(tab) >= 2
    if tab[1] < tab[0]:
        top = [tab[1], tab[0]]
    else:
        top = [tab[0], tab[1]]
    for i in range(2, len(tab)):
        #top contient les 2 plus gros éléments de tab[0:k] dans l'ordre croissant
        #on classe dans l'ordre croissant l'élément courant avec les 2 + gros
        top = [tab[i]] + top
        for j in range(2):
            if top[j] > top[j+1]:
                top[j], top[j+1] = top[j+1], top[j]
        top = top[1:] #on ne garde que les deux plus gros
    return top


#tests unitaires
assert deux_plus_grands([1,1]) == [1,1]
assert deux_plus_grands([2,1]) == [1,2]
assert deux_plus_grands([2,1, 2]) == [2,2]
assert deux_plus_grands([3,1, 2]) == [2,3]
assert deux_plus_grands([-3,-1, -2]) == [-2,-1]
assert deux_plus_grands([-3,1, -2]) == [-2,1]


#%% Exercice 10

# La fonction ci-dessous ne respecte pas sa spécification car elle ne compare que les deux dernières valeurs du tableau.

# In[17]:


def est_decroissant_fausse(tab:List[int])->bool:
    """
    Paramètre : un tableau tab d'entiers 
    Précondition : tab non vide
    Valeur renvoyée : un booléen
    Postcondition: détermine si tab dans l'ordre décroissant
    """
    i = len(tab) - 2
    while i >= 0:
        if tab[i] >= tab[i+1]:
            return True
        else:
            return False
        i = i - 1
        
        
#tests unitaires
assert  not est_decroissant_fausse([1,4,2])


def est_decroissant_while(tab:List[int])->bool:
    """
    Paramètre : un tableau tab d'entiers 
    Précondition : tab non vide
    Valeur renvoyée : un booléen
    Postcondition: détermine si tab dans l'ordre décroissant
    """
    i = len(tab) - 2
    while i >= 0:
        if tab[i] < tab[i+1]:
            return False
        i = i - 1
    return i < 0
        
        
#tests unitaires
assert  not est_decroissant_while([1,4,2])
assert  est_decroissant_while([1,1,1])
assert  est_decroissant_while([3,2,1])
assert  est_decroissant_while([3,3,1])
assert  not est_decroissant_while([1,2,3])
assert  est_decroissant_while([-1,-2,-2])



def est_decroissant_for(tab:List[int])->bool:
    """
    Paramètre : un tableau tab d'entiers 
    Précondition : tab non vide
    Valeur renvoyée : un booléen
    Postcondition: détermine si tab dans l'ordre décroissant
    """
    for i in range(len(tab) - 1):
        if tab[i] < tab[i+1]:
            return False
    return True

        
#tests unitaires
assert  not est_decroissant_for([1,4,2])
assert  est_decroissant_for([1,1,1])
assert  est_decroissant_for([3,2,1])
assert  est_decroissant_for([3,3,1])
assert  not est_decroissant_for([1,2,3])
assert  est_decroissant_for([-1,-2,-2])


## Le tri par bulles

#%%  Exercice 11

# In[26]:


def permuter(tab, i, j):
    """Paramètres :tab un tableau,  i et j des entiers
    Valeur renvoyée : aucun (None par défaut)
   	procédure qui permute les valeurs tab[i]  et tab[j]"""
    n = len(tab)    
    #à compléter
    #BEGIN CUT
    assert (0<=i and i<n) and (0<=j and j<n)   #précondition    
    tab[i], tab[j] = tab[j], tab[i]
    #END CUT
      
t1 = [11 ,  13 , 12]
permuter(t1, 1, 2)
#tests unitaires
assert t1 == [11, 12, 13] 
permuter(t1, 0, 0)
assert t1 == [11, 12, 13] 
permuter(t1, 0, 2)
assert t1 == [13, 12, 11] 


# Le rôle de la précondition dans le corps de la fonction  est de vérifier que les index `i` et `j` 
# passés en paramètres sont légitimes : entre 0 et `len(tab)-1` compris.

#%%
# On donne une première implémentation du tri par bulles sous la forme d'une procédure qui trie en place le tableau passé en paramètre :

def tri_bulle1(tab):
    n = len(tab)
    for i in range(n):
        for j in range(n - 1):
            if tab[j] > tab[j + 1]:
                permuter(tab, j, j + 1)   


# ![tri_bulle](images/tri_bulle1.png)

# Dans le tableau d'état précédent, on observe certains tours de boucles internes inutiles.  Si `tab` est de taille $n$, $n(n-1)$ comparaisons sont effectuées.
# 
# 
# 
# Comment  modifier le code de la  procédure `tri\_bulle1(tab)` pour que seulement $n-1 + n-2+ \ldots + 1=\frac{n(n-1)}{2}$ comparaisons soient effectuées pour `tab` de taille $n$ et donc $3$ comparaisons si `tab` de valeur `[4, 3, 2]` ?
# 
# A chaque tout de boucle externe on fait remonter jusqu'à sa position le plus grand élément parmi les éléments non classés restants qui sont au nombre de $n-i$ pour le tour de boucle externe d'index $i$. On peut donc remplacer la borne supérieur du `range` de la boucle interne par $n-1-i$.
# 

# In[29]:


def tri_bulle2(tab):
    #à compléter
    #BEGIN CUT
    n = len(tab)
    for i in range(n):
        for j in range(n - 1 - i):
            if tab[j] > tab[j + 1]:
                permuter(tab, j, j + 1)  
    #END CUT


# Dans l’exemple d’exécution ci-dessous, on voit que l’algorithme peut s’arrêter lorsque
# aucune « bulle » n’est remontée lors du dernier balayage

# ![tri_bulle](images/tri_bulle2.png)

# Compléter une troisième implémentation de la procédure de tri par bulles pour que le tri s'arrête si aucune permutation n'a été effectuée lors du dernier parcours du tableau.

# In[27]:


def tri_bulle3(tab):
    n, i, permutation  = len(tab), 0, True
    while permutation:
        "à compléter"
        #BEGIN CUT
        permutation = False
        for j in range(n - 1 - i):
            if tab[j] > tab[j + 1]:
                permuter(tab, j, j + 1)
                permutation = True
        i = i + 1
        #END CUT


## Exercices d'entraînement

#%% Exercice 12

# source : <https://www.codingame.com/ide/puzzle/minimal-number-of-swaps>

from typing import List

def precondition(tab:List[int])->bool:
    for e in tab:
        if e != 0 and e != 1:
            return False
    return True

def nombre_minimal_echanges(tab:List[int])->int:
    """
    Paramètres : tab un tableau d'entiers 
    Précondition : tab non vide et contient uniquement des 0 ou des 1
    Valeur renvoyée : un entier
    Postcondition : la valeur renvoyée est le nombre minimal d'échanges
    pour regrouper tous les 1 au début du tableau
    """
    assert len(tab) > 0 and precondition(tab)
    gauche = 0
    droite = len(tab) - 1
    echange = 0
    while gauche < droite:
        #variant de boucle : droite - gauche - 1
        #invariant de boucle : tab[:gauche] == [1] * gauche et tab[droite:] == [0] * (len(tab)  - droite)
        if tab[gauche] == 1:
            gauche = gauche + 1
        elif tab[droite] == 0:
            droite = droite - 1
        else:
            tab[gauche], tab[droite] = tab[droite], tab[gauche]
            echange = echange + 1
            gauche = gauche + 1
            droite = droite - 1
    return echangez


#%% Exercice 13

# Ecrire une fonction prenant un entier n en entrée, et renvoyant le tableau de n + 1 éléments contenant
# les termes de la suite de Fibonacci $f_0 , f_1 , ..., f_n$ :

from typing import List

def fibonacci(n:int)->List[int]:
    """
    Paramètres :n un entier
    Précondition : n >= 1
    Valeur renvoyée : un tableau d'entiers de taille n + 1
    Postcondition : renvoie un tableau avec les termes de f(0) à
    f(n) pour la suite de Fibonacci
    """
    assert n >= 1
    tab = [0, 1]
    for _ in range(n - 1):
        tab = tab + [tab[-1] + tab[-2]]
    return tab

#tests unitaires
assert fibonacci(1) == [0,1]
assert fibonacci(10) == [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]


#%% Exercice 14


from typing import List

def ligne_binome(n:int)->List[int]:
    """
    Paramètres :n un entier
    Précondition : n >= 0
    Valeur renvoyée : un tableau d'entiers de taille n + 1
    Postcondition : renvoie un tableau avec les coefficients
    binomiaux binom(n,k) 
    """
    assert n >= 0
    #on utilise un seul tableau
    #et on remplit de droite à gauche
    ligne = [1]
    for i in range(1, n + 1):
        ligne = [1] + ligne[1:i] + [1]
        for j in range(i - 1, 0, -1):
            ligne[j] = ligne[j] + ligne[j-1]
    return ligne

#Tests unitaires
assert ligne_binome(0) == [1]
assert ligne_binome(1) == [1,1]
assert ligne_binome(2) == [1,2,1]
assert ligne_binome(4) == [1, 4, 6, 4, 1]


#%% Exercice 15

# Écrire une fonction prenant en entrée un tableau d’entiers, et renvoyant la plus grande suite croissante
# constituée de termes consécutifs du tableau :


from typing import List

def plus_grand_bloc_croissant(tab:List[int])->List[int]:
    """
    Paramètres : un tableau d'entiers 
    Précondition :len(tab) >= 0
    Valeur renvoyée : un tableau d'entiers
    Postcondition : le plus grand sous-tableau d'éléments consécutifs croissants
    """
    assert len(tab) >= 0
    bloc_croissant = [tab[0]]
    lmax = len(bloc_croissant)
    #attention au partage de référence
    #on fait une copie surperficielle du bloc
    bloc_max = bloc_croissant[:]  
    for i in range(1, len(tab)):
        if tab[i] >= bloc_croissant[-1]:
            bloc_croissant.append(tab[i])
        else:
            bloc_croissant = [tab[i]]
        if len(bloc_croissant)> lmax:
            lmax = len(bloc_croissant)
            #attention au partage de référence
            #on fait une copie surperficielle du bloc
            bloc_max = bloc_croissant[:]
    return bloc_max
    
#Tests unitaires
assert plus_grand_bloc_croissant([2, 10, 1, 3, 5, 7, 6, 8, 9, 10]) == [1, 3, 5, 7]
assert plus_grand_bloc_croissant([1,1,1]) == [1,1,1]
assert plus_grand_bloc_croissant([3,2,1]) == [3]

