#!/usr/bin/env python
# coding: utf-8



#%% Exercice 1
def somme_puissance(a:int, b:int, n:int)->int:
    """Renvoie a**n + (a+1) ** n + .... + b ** n"""
   #àcompléter



#%% Exercice 6

from typing import List

def vsom(tab1:List[int], tab2:List[int])->List[int]:
    """
    Paramètres :
        tab1 et tab2 des tableaux de type list contenant des  entiers de type int
    Préconditions :
        tab1 non vide et len(tab1) == len(tab2)
    Valeur renvoyée :
        un tableau de type list contenant des  nombres de type int
    Postcondition :
        le tableau renvoyé est constitué des sommes
        terme à terme des éléments de tab1 et tab2        
    """
    assert len(tab1) > 0 and len(tab1) == len(tab2), "tab1 et tab2 doivent être de même longueur"
    #à compléter

#%% Exemple de code avec annotations de variant de boucle

def somme_tab2(tab:List[int])->int:
	"""Renvoie la somme des éléments d'un tableau non vide"""
	assert len(tab) > 0
	s = 0
	i = 0
	while i < len(tab):
		#variant de boucle : len(tab) - i
		s = s + tab[i]
		i = i + 1
	#sortie de boucle : i == len(tab) et len(tab) - i <= 0
	return s


#%% Exercice 8

def appartient(val:int,tab:List[int])->bool:
	"""
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un booléen
    Postcondition : renvoie la valeur de  val in tab
    """
	assert len(tab) > 0
	#à compléter

def index(val:int,tab:List[int])->int:
	"""
    Paramètres : val valeur de type int et tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie l'index de la première occurrence de val dans tab et len(tab) si val not in tab
    """
	assert len(tab) > 0
	#à compléter



#%% Exercice 9

def valeur_maximum(tab:List[int])->int:
    """
    Paramètres : tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie la valeur de max(tab)
    """
    #à compléter
    
    
def index_maximum(tab:List[int])->int:
	"""
    Paramètres :  tab tableau  d'entiers de type int
    Précondition : tab non vide
    Valeur renvoyée :  un entier
    Postcondition : renvoie l'index de la première occurence du maximum de tab
    """
	#à compléter
    
    
#%% Exercice 11

def permuter(tab, i, j):
    """Paramètres :tab un tableau,  i et j des entiers
    Valeur renvoyée : aucun (None par défaut)
   	procédure qui permute les valeurs tab[i]  et tab[j]"""
    n = len(tab)    
    assert (0<=i and i<n) and (0<=j and j<n)   #précondition
    tab[i], tab[j] = tab[j], tab[i]
      
t1 = [11 ,  13 , 12]
permuter(t1, 1, 2)
#tests unitaires
assert t1 == [11, 12, 13] 


def tri_bulle1(tab):
    n = len(tab)
    for i in range(n):
        for j in range(n - 1):
            if tab[j] > tab[j + 1]:
                permuter(tab, j, j + 1)   

#%% Exercice 12
def nombre_minimal_echanges(tab:List[int])->int:
    """
    Paramètres : tab un tableau d'entiers 
    Précondition : tab non vide et contient uniquement des 0 ou des 1
    Valeur renvoyée : un entier
    Postcondition : la valeur renvoyée est le nombre minimal d'échanges
    pour regrouper tous les 1 au début du tableau
    """
    #à compléter





