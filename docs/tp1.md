---
title: TP1
---


* [Énoncé](tp1/I1-TP01-sujet.pdf)
* [Corrigé version `.pdf`](tp1/Correction_TP1.pdf)
* [Corrigé version `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/4f92-89034)
* [Corrigé version `.py`](tp1/Correction_TP1.py)
