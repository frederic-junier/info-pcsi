---
title: TP4
---


* [Énoncé](tp4/I1-TP04-sujetV2.pdf)
* [Archive avec le matériel](tp4/squelette/tp4.zip)
* [Corrigé version `.pdf`](tp4/correction_tp4.pdf)
* [Corrigé version `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/9992-160136)
* [Corrigé version `.py`](tp4/correction_tp4.py)

