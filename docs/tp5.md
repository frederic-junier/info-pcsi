---
title: TP5
---


* [Énoncé](tp5/I1-TP05-Images-sujet.pdf)
* [Archive avec le matériel](tp5/materiel_tp5.zip)
* [Squelette de code `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/576a-205485)
* [Corrigé version `.pdf`](tp5/I1-TP05-Images-Correction.pdf)
* [Corrigé version `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/5e9b-205482)
* [Corrigé version `.py`](tp5/I1-TP05-Images-Correction.py)

