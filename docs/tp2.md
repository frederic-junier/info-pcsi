---
title: TP2
---


* [Énoncé](tp2/I1-TP02-sujet.pdf)
* [squelette de code](tp2/squelette/tp2_squelette.py)
* [fichier de tests, exo 12](tp2/test_nombre_minimal_echanges.py)
* [Corrigé version `.pdf`](tp2/Correction_TP2.pdf)
* [Corrigé version `.ipynb` sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/3a1e-89032)
* [Corrigé version `.py`](tp2/Correction_TP2.py)
